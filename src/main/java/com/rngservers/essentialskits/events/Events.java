package com.rngservers.essentialskits.events;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

import com.rngservers.essentialskits.Main;
import com.rngservers.essentialskits.gui.GUI;
import com.rngservers.essentialskits.kit.KitManager;

public class Events implements Listener {
	private Main plugin;
	private KitManager kits;

	public Events(Main plugin, KitManager kits) {
		this.plugin = plugin;
		this.kits = kits;
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		if (event.getMessage().toLowerCase().equals("/kit")) {
			if (!player.hasPermission("essentialskits.kitsgui") || !player.hasPermission("essentials.kit")) {
				return;
			}
			if (plugin.getConfig().getBoolean("settings.overrideKit")) {
				kits.openKitList(player);
				event.setCancelled(true);
			}
		}
		if (event.getMessage().toLowerCase().equals("/kits")) {
			if (!player.hasPermission("essentialskits.kitsgui") || !player.hasPermission("essentials.kit")) {
				return;
			}
			if (plugin.getConfig().getBoolean("settings.overrideKits")) {
				kits.openKitList(player);
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		String title = event.getView().getTitle();
		Player player = (Player) event.getView().getPlayer();
		// Preview
		if (title.startsWith("§r§r§r§6§r§2§r")) {
			if (!event.getSlotType().equals(SlotType.CONTAINER)) {
				event.setCancelled(true);
				return;
			}
			kits.openKitList(player);
			event.setCancelled(true);
		}
		// List
		if (title.startsWith("§r§r§r§6§r§0")) {
			event.setCancelled(true);
			if (!event.getSlotType().equals(SlotType.CONTAINER)) {
				return;
			}
			if (event.getRawSlot() >= event.getInventory().getSize()) {
				return;
			}

			// Kit List
			ItemStack item = event.getInventory().getItem(event.getRawSlot());
			if (item == null || item.getType().equals(Material.AIR)) {
				return;
			}

			NamespacedKey key = new NamespacedKey(plugin, "es-kitname");
			String kitName = item.getItemMeta().getPersistentDataContainer().get(key, PersistentDataType.STRING);
			if (event.getClick().equals(ClickType.LEFT)) {
				player.performCommand("essentials:kit " + kitName);
			}

			if (event.getClick().equals(ClickType.RIGHT)) {
				if (!plugin.getConfig().getBoolean("settings.enablePreview")) {
					return;
				}
				kits.loadKits();
				List<ItemStack> itemList = new ArrayList<ItemStack>();
				for (String itemString : kits.getItemStrings(kitName)) {
					ItemStack display = kits.generateItemStack(itemString);
					if (display != null) {
						itemList.add(display);
					}
				}
				String guiName = plugin.getConfig().getString("gui.previewTitle").replace("$name", kitName).replace("&",
						"§");
				GUI gui = new GUI(kits, "§r§r§r§6§r§2§r" + guiName, itemList, 36);
				gui.openInventory(player);
			}
			if (event.getClick().equals(ClickType.SHIFT_LEFT) || event.getClick().equals(ClickType.SHIFT_RIGHT)) {
				if (!player.hasPermission("essentialskits.editkit")) {
					return;
				}
				kits.loadKits();
				List<ItemStack> itemList = new ArrayList<ItemStack>();
				for (String itemString : kits.getItemStrings(kitName)) {
					ItemStack display = kits.generateItemStack(itemString);
					if (display != null) {
						itemList.add(display);
					}
				}
				GUI gui = new GUI(kits, "§r§r§r§6§r§1§r" + kitName + " Editor", itemList, 36);
				gui.openInventory(player);
			}
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		String title = event.getView().getTitle();
		if (!title.startsWith("§r§r§r§6§r§1")) {
			return;
		}
		title = ChatColor.stripColor(title).replace(" Editor", "");
		Player player = (Player) event.getPlayer();
		if (!player.hasPermission("essentialskits.editkit")) {
			return;
		}
		List<String> items = new ArrayList<String>();
		for (ItemStack item : event.getInventory()) {
			if (item != null) {
				String fullItem = kits.generateItemString(item);
				items.add(fullItem);
			}
		}
		if (items.isEmpty()) {
			return;
		}
		String[] parts = title.split(" ");
		kits.saveKit(parts[0], items);
		kits.reloadKits();
		player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
				+ ChatColor.GOLD + " " + parts[0] + ChatColor.RESET + " kit saved, Essentials reloaded!");
	}
}
